'use strict';

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }
    keys.push.apply(keys, symbols);
  }
  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }
  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {value: value, enumerable: true, configurable: true, writable: true});
  } else {
    obj[key] = value;
  }
  return obj;
}


jQuery(document).ready(function ($) {
  $('.custom-select').each(function (index, item) {
    var selectParentNode = $(item).parents('.field-wrapper') || $(item).parent();
    $(item).select2({
      dropdownParent: selectParentNode,
      dropdownPosition: 'below'
    });
  });
  var modals = $('.modal-wrapper'),
    productDescription = $("#product-description") || null;
  var Slick = {
    options: {
      defaultSlickOptions: {
        touchThreshold: 20,
        mobileFirst: true,
        arrows: true,
        dots: false,
        swipe: true,
        swipeToSlide: true,
        infinite: false
      },
      GetHeroSectionOptions: function GetHeroSectionOptions() {
        return _objectSpread({}, this.defaultSlickOptions, {}, {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          autoplay: true,
          speed: 800,
          autoplaySpeed: 4000,
          infinite: true,
          responsive: [{
            breakpoint: 993,
            settings: {
              arrows: true
            }
          }]
        });
      },
      GetFashionIndexPageOptions: function GetFashionIndexPageOptions() {
        return _objectSpread({}, this.defaultSlickOptions, {}, {
          variableWidth: true
        });
      },
      GetFashionPageOptions: function GetFashionPageOptions() {
        return _objectSpread({}, this.defaultSlickOptions, {}, {
          responsive: [{
            breakpoint: 420,
            settings: {
              slidesToShow: 2
            }
          }, {
            breakpoint: 640,
            settings: {
              slidesToShow: 3
            }
          }, {
            breakpoint: 991,
            settings: {
              slidesToShow: 1
            }
          }]
        });
      }
    },
    methods: {
      FashionSliderCounter: function FashionSliderCounter(slider) {
        var _slider = $(slider.selector);

        var currentSlide = document.getElementById('current') || null,
          targetSlickTrack = $(_slider).find('.slick-list'),
          totalSlides = document.getElementById('total') || null;

        var SetFashionSliderCount = function SetFashionSliderCount(index) {
          if (currentSlide) {
            currentSlide.textContent = "".concat(++index);
            totalSlides.textContent = $(_slider).find('li').length;
          }
        };

        var UpdateFashionSliderWidth = function UpdateFashionSliderWidth() {
          var windowWidth = $(window).width();

          if (targetSlickTrack) {
            var slickTrackStyle = windowWidth > 768 ? {
              paddingLeft: "".concat(windowWidth * 0.34, "px")
            } : {
              paddingLeft: 30
            };
            $(targetSlickTrack).css(slickTrackStyle);
          }
        };

        SetFashionSliderCount(0);
        UpdateFashionSliderWidth();
        $(_slider).on('afterChange', function (event, slick, currentSlide) {
          SetFashionSliderCount(currentSlide);
          ;
        });
        $(window).resize(function () {
          UpdateFashionSliderWidth();
        });
      },
      SliderInit: function SliderInit(slider, options) {
        if ($(slider)) {
          $(slider).not('.slick-initialized').slick(options);
        }
      },
      ProductSliderManager: function ProductSliderManager(slider, opt) {
        var windowWidth = $(window).width();

        if (windowWidth < 993) {
          Slick.methods.SliderInit(slider, opt);
        } else if ($(slider).is('.slick-initialized')) {
          $(slider).slick('destroy');
        }
      }
    },
    GetSliders: function GetSliders() {
      var options = this.options;
      return [{
        name: "hero",
        selector: "#hero-slider",
        opt: options.GetHeroSectionOptions()
      }, {
        name: "fashionIndexPage",
        selector: "#fashion-slider",
        opt: options.GetFashionIndexPageOptions()
      }, {
        name: "fashionPage",
        selector: '#fashion-page-slider',
        opt: options.GetFashionPageOptions()
      }, {
        name: "product",
        selector: '.product-photo-list',
        opt: options.GetFashionPageOptions()
      }];
    },
    init: function init() {
      var those = Slick,
        sliders = those.GetSliders(),
        productSlider = sliders.filter(function (slider) {
          return slider.name === "product";
        }); // get functions

      var InitSliders = those.methods.SliderInit,
        SliderCounter = those.methods.FashionSliderCounter,
        ProductManager = those.methods.ProductSliderManager; // call

      sliders.forEach(function (slider) {
        var name = slider.name,
          selector = slider.selector,
          opt = slider.opt;

        switch (name) {
          case "product":
            ProductManager(selector, opt);
            break;

          case "fashionIndexPage":
            InitSliders(selector, opt);
            SliderCounter(slider);
            break;

          default:
            InitSliders(selector, opt);
            break;
        }
      });
      $(window).resize(function () {
        var _productSlider$ = productSlider[0],
          selector = _productSlider$.selector,
          opt = _productSlider$.opt;
        ProductManager(selector, opt);
        sliders.forEach(function (slider) {
          if ($(slider).is('.slick-initialized')) {
            $(slider.selector).slick('resize');
          }
        });
      });
    }
  };
  var Modal = {
    show: function show(modal) {
      $(modals).fadeIn('50', function () {
        $(modal).addClass('opened');
      });
    },
    hide: function hide(modal) {
      return $(modal).removeClass('opened');
    },
    destroy: function destroy() {
      Modal.hide($('.modal.opened'));
      setTimeout(function () {
        return $(modals).fadeOut('fast');
      }, 350);
    },
    init: function init() {
      var those = this,
        show = those.show,
        hide = those.hide,
        destroy = those.destroy,
        showTriggers = [$('.show-modal')],
        destroyTriggers = [$('.close-modal'), $(modals)];
      $(showTriggers).each(function () {
        $(this).click(function (e) {
          var modal = $(this).attr('data-target'),
            otherModals = $('.modal').not(modal);

          if ($(otherModals).hasClass('opened')) {
            hide(otherModals);
            setTimeout(function () {
              return show(modal);
            }, 150);
          } else {
            show(modal);
          }
        });
      });
      $(destroyTriggers).each(function () {
        $(this).click(function (e) {
          var denied = $('.modal');

          if (!denied.is(e.target) && denied.has(e.target).length === 0) {
            destroy();
          } else if ($(e.target).is('.close-modal') || $(e.target).is($('.close-modal').children())) {
            destroy();
          }
        });
      });
      $('body').keydown(function (e) {
        if (e.keyCode === 27) {
          hide($('.modal.opened'));
          setTimeout(function () {
            return destroy();
          }, 350);
        }
      });
    }
  };
  var Sticky = {
    elements: [{
      selector: $("#filter-content") || null,
      parent: ".filter-sidebar",
      offset: 30
    }, {
      selector: $('.previews-list') || null,
      parent: ".previews-inner",
      offset: 30
    }, {
      selector: $('.product-content') || null,
      parent: ".product-sticky-wrapper",
      offset: 30
    }],
    init: function init() {
      var StickyInit = function StickyInit(selector, parent) {
        var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 30;

        if (selector !== null) {
          $(selector).stickybits({
            scrollEl: parent,
            useStickyClasses: true,
            stickyBitStickyOffset: offset
          });
        }
      };

      this.elements.forEach(function (stickyEl) {
        var selector = stickyEl.selector,
          parent = stickyEl.parent,
          offset = stickyEl.offset;
        StickyInit(selector, parent, offset);
      });
    }
  };

  function ToggleMenu(element, isShow) {
    if (isShow) {
      $(element).addClass('opened');
    } else {
      $(element).removeClass('opened');
    }
  }

  $('.show-menu').click(function () {
    if ($(this).hasClass('opened')) {
      ToggleMenu($("#main-menu"), false);
      $(this).removeClass('opened');
    } else {
      ToggleMenu($("#main-menu"), true);
      $(this).addClass('opened');
    }
  });
  $('.submenu-trigger').click(function () {
    var target = $(this).data('target');
    ToggleMenu($(target), true);
  });
  $('.back-menu').click(function () {
    ToggleMenu($(this).parents('.submenu'), false);
  });
  var isHover = false;
  $(".submenu-trigger").hover(function () {
    var target = $(this).data('target');
    ToggleMenu($(target), true);
    isHover = true;
  }, function () {
    var target = $(this).data('target');

    if (isHover) {
      $('body').mousemove(function (e) {
        var deniedArr = [".submenu-trigger", "#".concat($(target).attr('id'))];
        var status = true;

        for (var i = 0; i < deniedArr.length; i++) {
          var denied = $(deniedArr[i]);

          if (denied.is(e.target) || denied.has(e.target).length !== 0) {
            status = false;
          }
        }

        if (status) {
          ToggleMenu($(target), false);
          $('body').off('mousemove');
        }
      });
    }
  }); // **** INIT FUNCTIONS *** //

  Slick.init();
  Modal.init();
  Sticky.init(); // **** HIDE FILTER SIDEBAR ON MOB VERSION *** //

  $('#hide-sidebar').click(function () {
    var sideBar = $(this).parents('aside');
    $(sideBar).slideUp('fast');
  }); // **** HIDDEN LIST *** //

  var LimitHeightHandler = function LimitHeightHandler(block) {
    var button = $(block).find('.more-button'),
      toHidden = $(block).find('.wrapper--hidden'),
      realHeight = parseFloat($(toHidden).css('height')),
      maxHeight = $(window).width() > 540 ? 75 : 100;
    var buttonText = {
      closed: $(button).html() || "Подробнее",
      opened: $(button).attr('data-open-text') || "Скрыть"
    };
    var isCloset = false;

    var heightToggle = function heightToggle() {
      $(toHidden).css({
        "height": "".concat(isCloset ? realHeight : maxHeight, "px")
      });
      isCloset = !isCloset;
      $(button).html(isCloset ? buttonText.closed : buttonText.opened);
    };

    heightToggle();
    $(button).click(heightToggle);
  };

  if (productDescription !== null) {
    LimitHeightHandler(productDescription);
  } // **** ANCHOR *** //


  var anchorNode = $('.anchor'),
    anchor = $(anchorNode).is('a') ? $(anchorNode) : $(anchorNode).find('a');
  $(anchor).click(function (e) {
    e.preventDefault();
    var targetId = $(this).attr('href'),
      scrollTop = $(targetId).offset().top;
    $('html, body').animate({
      scrollTop: scrollTop - 30
    }, 400);
  }); // **** PRODUCT COUNTER *** //

  var productCounter = $('#product-counter') || null,
    decr = $(productCounter).find("#minus") || null,
    incr = $(productCounter).find("#plus") || null,
    output = $(productCounter).find("input") || null,
    min = parseInt($(output).attr('min')),
    max = parseInt($(output).attr('max'));
  $(output).change(function (e) {
    var output = $(e.target),
      value = $(output).val();

    if (value < min) {
      $(output).val(min);
    } else if (value > max) {
      $(output).val(max);
    }
  });
  $(decr).click(function () {
    var value = parseInt($(output).val());
    if (value === min) return false;
    $(output).val(--value);
  });
  $(incr).click(function () {
    var value = parseInt($(output).val());
    if (value === max) return false;
    $(output).val(++value);
  });

  function PlayIframe() {
    var iframeId = "#about-video";
    var parent = $(iframeId).parents('.video-inner'),
      img = $(parent).find('img'),
      button = $(parent).find('button');

    var play = function play() {
      $(iframeId)[0].src += "?autoplay=1";
      $(img).fadeOut('fast');
      $(button).fadeOut('fast');
    };

    $(img).click(function () {
      play();
    });
    $(button).click(function () {
      play();
    });
  }

  PlayIframe();
});
