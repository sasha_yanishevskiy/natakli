'use strict';
/* if work with html set TRUE, else - FALSE */
const htmlOWp = false;


/* import dependencies */
import config from 'config';
import gulp from 'gulp';
import browserSync from 'browser-sync';

/* PostCSS plugins */
import postcssPresetEnv from 'postcss-preset-env';
import cssnano from 'cssnano';
import urlrev from 'postcss-urlrev';
import discardDuplicates from 'postcss-discard-duplicates';
import discardEmpty from 'postcss-discard-empty';
import combineDuplicatedSelectors from 'postcss-combine-duplicated-selectors';
import mqpacker from 'css-mqpacker'; // install via yarn
import charset from 'postcss-single-charset';
import willChangeTransition from 'postcss-will-change-transition';
import willChange from 'postcss-will-change';
import momentumScrolling from 'postcss-momentum-scrolling';
import webpcss from 'webpcss';


import replace from 'gulp-replace';

const extReplace = require("gulp-ext-replace");
const imageminWebp = require("imagemin-webp");
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminAdvpng = require('imagemin-advpng');
const imageminGuetzli = require('imagemin-guetzli');

const plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*'],
    replaceString: /\bgulp[\-.]/
});

config.path.base.wp = './wp-content/themes/' + config.theme + '/';
ChangeBasePath(config);
config.path.base.dest = config.path.base.wp;

let processors = [
    charset(),
    willChangeTransition(),
    willChange(),
    momentumScrolling([
        'scroll'
    ]),
    discardDuplicates(),
    discardEmpty(),
    combineDuplicatedSelectors({
        removeDuplicatedProperties: true
    }),
];

let processorsAfter = [
    urlrev(),
    postcssPresetEnv({
        stage: 4,
        warnForDuplicates: false
    }),
    cssnano({
        preset: 'advanced',
        reduceIdents: true,
        zindex: false
    })
];

let processorsDisableZIndex = [
    urlrev(),
    postcssPresetEnv({
        stage: 4,
        warnForDuplicates: false
    }),
    cssnano({
        preset: 'default',
        reduceIdents: true,
        zindex: false
    })
];

let processorsAfterSCSS = [
    urlrev(),
    postcssPresetEnv({
        stage: 4,
        warnForDuplicates: false
    }),
    cssnano({
        preset: 'advanced',
        reduceIdents: true,
        zindex: false
    })
];

/* separated styles */
gulp.task('styles_base', function () {
    let templateName = 'style',
        source = 'assets/scss/' + templateName + '.scss',
        destination = config.path.styles.dest;
    return gulp.src(source)
        .pipe(customPlumber('Error Running Sass'))
        .pipe(plugins.sass({
            outputStyle: 'compact',
            precision: 5,
            onError: console.error.bind(console, 'Sass error:')
        }))
        .pipe(plugins.postcss(processors))
        .pipe(gulp.dest(destination))
        .pipe(plugins.size({
            showFiles: true,
            title: 'task: ' + templateName
        }))
        .pipe(browserSync.reload({stream: true}));
});


// Optimize images
gulp.task('image', function () {
    return gulp
        .src(config.path.images.srcimg)
        .pipe(plugins.newer(config.path.images.dest))
        .pipe(plugins.if(
            '*.png',
            plugins.imagemin([
                imageminPngquant(),
                imageminAdvpng(),
            ])
        ))
        .pipe(plugins.if(
            '*.jp*g',
            plugins.imagemin([
                imageminMozjpeg({progressive: true}),
                imageminGuetzli({quality: 95}),
            ])
        ))
        .pipe(plugins.if(
            '*.gif',
            plugins.imagemin([
                plugins.imagemin.gifsicle({
                    interlaced: true
                }),
            ])
        ))
        .pipe(plugins.if(
            '*.svg',
            plugins.imagemin([
                plugins.imagemin.svgo({
                    plugins: [{
                        removeViewBox: true
                    }]
                })
            ])
        ))
        .pipe(plugins.size({
            showFiles: true,
            title: 'task:image: '
        }))
        .pipe(gulp.dest(config.path.images.dest))
        .pipe(browserSync.reload({stream: true}));
});

// Optimize images to WebP
gulp.task("image2webp", function () {
    return gulp.src([config.path.images.dest + '**/*.{png,jpg,jpeg}'])
        .pipe(plugins.imagemin([
            imageminWebp({
                quality: 95
            })
        ]))
        .pipe(extReplace(".webp"))
        .pipe(gulp.dest(config.path.images.dest));
});

// Copy web fonts to dist
gulp.task('fonts', function () {
    return gulp
        .src(config.path.fonts.src)
        .pipe(plugins.newer(config.path.fonts.dest))
        .pipe(gulp.dest(config.path.fonts.dest))
        .pipe(plugins.size({
            showFiles: true,
            title: 'task:fonts'
        }));
});

// Optimize script
const scripts_arr = [
    'assets/js/main.js'
];
gulp.task('scripts_base', function () {
    return gulp
        .src(scripts_arr)
        .pipe(plugins.size({
            showFiles: true,
            title: 'task:scripts'
        }))
        .pipe(plugins.newer(config.path.scripts.dest))
        .pipe(customPlumber('Error Compiling Scripts'))
        .pipe(plugins.if('*.js', plugins.uglify()))
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(gulp.dest(config.path.scripts.dest))
        .pipe(plugins.size({
            showFiles: true,
            title: 'task:scripts_base'
        }))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('version', function () {
    const today = new Date();
    const version = today.getDate() + today.getHours();
    const file_folder = './wp-content/themes/natakli/inc/';
    const file = file_folder + './defines.php';
    console.log('New .css and .js version is ' + version);
    return gulp
        .src(file)
        .pipe(customPlumber('Error Changing Site Styles & Scripts Version'))
        .pipe(replace(/(\'STYLES_VERSION\',\x20)(.+?)(\);)/g, '$1' + version + ');'))
        .pipe(replace(/(\'SCRIPTS_VERSION\',\x20)(.+?)(\);)/g, '$1' + version + ');'))
        .pipe(gulp.dest(file_folder))
});

gulp.task('images', gulp.series(
    'image',
    'image2webp',
));

gulp.task('styles', gulp.series(
    'styles_base',
    'version',
));

gulp.task('scripts', gulp.series(
    'scripts_base',
    'version'
));

/* browserSync config */
let args = {
    notify: false,
    port: 9090,
    proxy: config.domain,
    host: config.domain
}

gulp.task('default', gulp.parallel(
    gulp.series(
        'styles',
        'images'
    ),
    'scripts',
    'fonts',
));


// watch for changes
gulp.task('watch', function () {

    browserSync.init(args);

    gulp.watch(config.path.base.desthtml).on('change', browserSync.reload);

    gulp.watch(config.path.images.srcimg, gulp.series('image'));

    gulp.watch(scripts_arr, gulp.series('scripts_base'));

});


// Custom Plumber function for catching errors
function customPlumber(errTitle) {
    return plugins.plumber({
        errorHandler: plugins.notify.onError({
            // Customizing error title
            title: errTitle || 'Error running Gulp',
            message: 'Error: <%= error.message %>',
            sound: 'Bottle'
        })
    });
};
module.exports = customPlumber;

function ChangeBasePath(config) {
    config.path.images.dest = config.path.images.dest.replace(config.path.base.dest, config.path.base.wp);
    config.path.fonts.dest = config.path.fonts.dest.replace(config.path.base.dest, config.path.base.wp);
    config.path.styles.dest = config.path.styles.dest.replace(config.path.base.dest, config.path.base.wp);
    config.path.scripts.dest = config.path.scripts.dest.replace(config.path.base.dest, config.path.base.wp);
    config.path.base.desthtml = config.path.base.desthtml.replace(config.path.base.dest, config.path.base.wp);
}
