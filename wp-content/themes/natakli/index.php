<?php get_header() ?>
<div class="page-content">
<!-- CODE HERE -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php the_content() ?>
<?php endwhile; ?>
<?php endif; ?>
    <div class="clearfix"></div>
</div>

<?php get_footer() ?>
