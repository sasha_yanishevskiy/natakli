<?php // Template name: Catalogue  ?>
<?php get_header() ?>
<div class="page-content products-page">
  <section class="section section--catalogue">
    <div class="container max-container">
      <div class="row">
        <div class="col-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/dist">Дом бренда</a></li>
              <li class="breadcrumb-item active" aria-current="page">Каталог</li>
            </ol>
          </nav>
        </div>

        <div class="col-12">
          <div class="row align-items-center">
            <div class="col-12 col-md-6">
              <h1>Каталог NataKli</h1>
            </div>
            <div class="col-6 d-none d-md-block">
              <div class="size-select d-flex justify-content-end">
                <span>Размер: </span><a href="#">Xl</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container fluid-mobile">
      <div class="products">
        <ul class="products-list row">
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">
                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>

                <div class="price-wrapper">
                  <div class="price">€ 2 080,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
          <li class="col-6 col-md-4 col-lg-3">
            <div class="product-card">
              <div class="product-card--cover">

                <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                <div class="sale">-30%</div>

              </div>

              <div class="product-card--footer">
                <span>Дуальность</span>
                <h4>Платье из микрофлая</h4>
                <div class="price-wrapper">
                  <div class="price price--old">€ 2 080,00</div>
                  <div class="price">€ 1 456,00</div>
                </div>

                <a href="#" class="button button--bg--theme">Подробнее</a>
              </div>
            </div>
          </li>
        </ul>

        <button class="button button--bg--theme">Загрузить еще</button>
      </div>
    </div>
  </section>
</div>
<?php get_footer() ?>

