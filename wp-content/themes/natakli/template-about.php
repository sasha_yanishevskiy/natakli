<?php // Template name: About  ?>
<?php get_header() ?>
<div class="page-content about-block">
  <div class="container-fluid px-0">
    <div class="video-inner">
      <iframe id="about-video" width="100%" height="100%" src="https://www.youtube.com/embed/R_k24RUlKFE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <img src="<?php echo get_template_directory_uri() ?>/img/video-cover.jpg" alt="">
      <button type="button" id="about-video-play" class="play">
      </button>
    </div>
  </div>
  <div class="container fluid-mobile">
    <div class="about-content">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="about-text">
            <h1>
              Украинский бренд
              женской одежды “NataKli”
            </h1>

            <p>
              NataKli - это деловая изящность, мы создаем  одежду для энергичных леди.
              Продуманный дизайн, трепетное отношение к стилю и эстетике гарантирует качество и индивидуальность от NataKli.
              Наша первая коллекция наполнена смыслом,а каждая модель особым содержанием.
            </p>
            <p>
              NataKli - это деловой стиль и женственность, сдержанность и темперамент, уверенность и чувственность. Мы отличаемся интеллектуальным образом и элегантным силуэтом. Наш дизайнер идет в ногу с мировыми трендами и постоянно прислушивается к желаниям, мечтам и потребностям женщин.
              Также, в нашу коллекцию мы внесли нотки вневременной классики, которую можно носить из сезона в сезон: юбки -карандаши, строгие брюки, классические блузы и платья-футляры.
              Для создания наших шедевров мы предпочитаем использовать качественные, преимущественно натуральные ткани.
            </p>

            <p>
              Если вы в поиске изысканной классики с современными нотками - то Вам к нам!
            </p>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="pic-wrapper pic-wrapper--about-brand">
            <img src="<?php echo get_template_directory_uri() ?>/img/about-brand.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer() ?>

