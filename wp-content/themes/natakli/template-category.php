<?php // Template name: Category ?>
<?php get_header() ?>

<div class="page-content products-page">

  <div class="container max-container">
    <div class="row">
      <div class="col-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./index.html">Дом бренда</a></li>
            <li class="breadcrumb-item"><a href="./catalogue.html">Каталог</a></li>
            <li class="breadcrumb-item active" aria-current="page">Категория</li>
          </ol>
        </nav>
      </div>

      <div class="col-12">
        <div class="row align-items-center">
          <div class="col-12">
            <h1>Платья</h1>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="max-container fluid-mobile overflow-visible container">
    <div class="row">
      <div class="col-12 col-lg-4 col-xl-3">

        <aside class="filter filter-sidebar" id="filter-sidebar">
          <button id="hide-sidebar" class="mobile-sibar-manager">
            <span>Фильтры и сортровка</span>
            <img src="<?php echo get_template_directory_uri() ?>/img/icons/close.svg" alt="">
          </button>

          <div class="filter-content" id="filter-content">
            <form action="">
              <fieldset>
                <legend>
                  Упорядочить по
                </legend>

                <ul class="filters-list">
                  <li>
                    <div class="radio filter-item">
                      <input id="sorting_1" type="radio" name="sorting" value="new">
                      <label for="sorting_1"></label>
                      <label class="theme-label" for="sorting_1">Сначала новые</label>
                    </div>
                  </li>

                  <li>
                    <div class="radio filter-item">
                      <input id="sorting_2" type="radio" name="sorting" value="price-up">
                      <label for="sorting_2"></label>
                      <label class="theme-label"  for="sorting_2">Цены по возрастанию</label>
                    </div>
                  </li>

                  <li>
                    <div class="radio filter-item">
                      <input id="sorting_3" type="radio" name="sorting" value="price-down">
                      <label for="sorting_3"></label>
                      <label class="theme-label" for="sorting_3">Цены по убыванию</label>
                    </div>
                  </li>
                </ul>
              </fieldset>

              <fieldset class="fieldset">
                <legend>
                  Категория
                </legend>

                <ul class="filters-list">
                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="dresses-type" id="dresses-type_1">
                      <label for="dresses-type_1"></label>
                      <label class="theme-label" for="dresses-type_1">Все платья</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="dresses-type" id="dresses-type_2">
                      <label for="dresses-type_2"></label>
                      <label class="theme-label" for="dresses-type_2">Короткие платья</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="dresses-type" id="dresses-type_3">
                      <label for="dresses-type_3"></label>
                      <label class="theme-label" for="dresses-type_3">Длинные платья</label>
                    </div>
                  </li>
                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="dresses-type" id="dresses-type_4">
                      <label for="dresses-type_4"></label>
                      <label class="theme-label" for="dresses-type_4">Платья миди</label>
                    </div>
                  </li>
                </ul>
              </fieldset>

              <fieldset class="fieldset">
                <legend>
                  Размер
                </legend>

                <ul class="filters-list">
                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_1">
                      <label for="size_1"></label>
                      <label class="theme-label" for="size_1">Все размеры</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_2">
                      <label for="size_2"></label>
                      <label class="theme-label" for="size_2">XS</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_3">
                      <label for="size_3"></label>
                      <label class="theme-label" for="size_3">S</label>
                    </div>
                  </li>
                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_4">
                      <label for="size_4"></label>
                      <label class="theme-label" for="size_4">M</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_5">
                      <label for="size_5"></label>
                      <label class="theme-label" for="size_5">L</label>
                    </div>
                  </li>

                  <li>
                    <div class="filter-item checkbox">
                      <input type="checkbox" name="size" id="size_6">
                      <label for="size_6"></label>
                      <label class="theme-label" for="size_6">XL</label>
                    </div>
                  </li>
                </ul>
              </fieldset>

              <fieldset>
                <legend>
                  Цвет
                </legend>

                <ul class="filters-list filter-colors" id="filter-colors" >
                  <li>
                    <div class="radio filter-item">
                      <input id="color_1" type="radio" name="color" value="none">
                      <label for="color_1"></label>
                      <label class="theme-label" for="color_1" style="border: 1px solid #000;"></label>
                    </div>
                  </li>

                  <li>
                    <div class="radio filter-item">
                      <input id="color_2" type="radio" name="color" value="#FF0000">
                      <label for="color_2"></label>
                      <label class="theme-label"  for="color_2" style="background-color: #FF0000;"></label>
                    </div>
                  </li>

                  <li>
                    <div class="radio filter-item">
                      <input id="color_3" type="radio" name="color" value="#0085FF">
                      <label for="color_3"></label>
                      <label class="theme-label" for="color_3" style="background-color: #0085FF;"></label>
                    </div>
                  </li>
                  <li>
                    <div class="radio filter-item">
                      <input id="color_4" type="radio" name="color" value="#000000">
                      <label for="color_4"></label>
                      <label class="theme-label" for="color_4" style="background-color: #000;"></label>
                    </div>
                  </li>
                </ul>
              </fieldset>
            </form>
          </div>
        </aside>
      </div>

      <div class="col-12 col-lg-8 col-xl-9">
        <section class="section section--catalogue">

          <div class="products mw-100">
            <ul class="products-list row">
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">
                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>

                    <div class="price-wrapper">
                      <div class="price">€ 2 080,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">
                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>

                    <div class="price-wrapper">
                      <div class="price">€ 2 080,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">
                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>

                    <div class="price-wrapper">
                      <div class="price">€ 2 080,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>
              <li class="col-6 col-md-4 col-xl-3">
                <div class="product-card">
                  <div class="product-card--cover">

                    <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                    <div class="sale">-30%</div>

                  </div>

                  <div class="product-card--footer">
                    <span>Дуальность</span>
                    <h4>Платье из микрофлая</h4>
                    <div class="price-wrapper">
                      <div class="price price--old">€ 2 080,00</div>
                      <div class="price">€ 1 456,00</div>
                    </div>

                    <a href="#" class="button button--bg--theme">Подробнее</a>
                  </div>
                </div>
              </li>

            </ul>

            <button class="button button--bg--theme">Загрузить еще</button>
          </div>
        </section>
      </div>
    </div>
  </div>

</div>
<?php get_footer() ?>
