<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nata Kli</title>
  <?php wp_head() ?>
</head>
<body>
<header>
  <div class="submenu" id="submenu-catalogue">
    <div class="container-fluid px-0">
      <div class="submenu-wrapper">
        <div class="row">
          <div class="col-12 col-lg-7">
            <div class="submenu-nav">
              <div class="row no-gutters">
                <div class="col-12 col-sm-6 col-md-4">
                  <div class="submenu-block submenu-block--clothes">
                    <h3>Одежда</h3>
                    <div class="row no-gutters">
                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <ul class="submenu-block--list submenu-block--list--clothes">
                          <li><a href="#">Платья</a></li>
                          <li><a href="#">Жакеты</a></li>
                          <li><a href="#">Брюки</a></li>
                          <li><a href="#">Юбки</a></li>
                          <li><a href="#">Блузы</a></li>
                          <li><a href="#">Платья</a></li>
                          <li><a href="#">Жилеты</a></li>
                        </ul>
                      </div>
                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <ul class="submenu-block--list submenu-block--list--clothes">
                          <li><a href="#">Костюмы</a></li>
                          <li><a href="#">Корсеты</a></li>
                          <li><a href="#">Образы</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4">
                  <div class="submenu-block submenu-block--collections">
                    <h3>Коллекции</h3>
                    <div class="row no-gutters">
                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <ul class="submenu-block--list submenu-block--list--collections">
                          <li>
                            <a href="#">Дуальность</a>
                          </li>
                        </ul>
                      </div>

                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <!-- add here anything-->
                      </div>
                    </div>

                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4">
                  <div class="submenu-block submenu-block--offers">
                    <h3>Предложения</h3>
                    <div class="row no-gutters">
                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <ul class="submenu-block--list submenu-block--list--offers">
                          <li><a href="#">Новинки</a></li>
                          <li><a href="#">Акции</a></li>
                        </ul>
                      </div>
                      <div class="col-6 col-lg-12 col-xl-6 submenu-block-inner">
                        <!-- add here anything-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-5 d-none d-lg-block">
            <div class="header-banner">
              <img src="<?php echo get_template_directory_uri() ?>/img/baner.jpg" alt="Баннер">
            </div>
          </div>

          <div class="col-12 d-lg-none">
            <div class="submenu-footer">
              <div class="row">
                <div class="col-6">
                  <button class="back-menu ">
                    <img src="<?php echo get_template_directory_uri() ?>/img/icons/menu-back.svg" alt="Назад">
                  </button>
                </div>
                <div class="col-6 d-flex justify-content-end">
                  <span>Каталог</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <nav>
    <div class="container">

      <div class="row no-gutters">
        <!-- logo-->
        <div class="col-5 col-lg-2 d-flex">
          <a href="/" class="logo"> <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="Nata Kli">
          </a>
        </div>

        <!-- menu-->

        <div class="menu col-lg-7" id="main-menu">
          <button class="close close-menu d-none">x</button>
          <ul class="main-menu d-flex align-items-center">
            <li><a href="#">Дом бренда</a></li>
            <li data-target="#submenu-catalogue" class="submenu-trigger"><a href="#">Каталог</a></li>
            <li><a href="#">О бренде</a></li>
            <li><a href="#">Связаться с нами</a></li>
          </ul>

          <ul class="lang-list d-lg-none">
            <li>
              <a href="#">ru</a>
            </li>
          </ul>
        </div>

        <!--search, cart etc..-->

        <div class="col-7 col-lg-3 d-flex align-items-center justify-content-end">
          <div class="d-flex w-100 align-items-center justify-content-end">
            <ul class="project-actions d-flex align-items-center">
              <li>
                <button type="button">
                  <img src="<?php echo get_template_directory_uri() ?>/img/icons/search.svg" alt="Поиск" title="Поиск">
                </button>
              </li>

              <li>
                <button type="button" class="cart-button show-modal" data-target="#cart">
                  <img src="<?php echo get_template_directory_uri() ?>/img/icons/cart.svg" alt="Корзина"> <span>5</span>
                </button>
              </li>
            </ul>

            <!-- lang-->

            <ul class="lang-list d-none d-lg-block">
              <li>
                <a href="#">ru</a>
              </li>
            </ul>

            <!-- burger -->

            <div class="d-block d-lg-none">
              <button type="button" class="hamburger show-menu">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
              </button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </nav>


</header>
<main>
