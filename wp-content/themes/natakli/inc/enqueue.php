<?php
add_action( 'wp_enqueue_scripts', 'sol_scripts', 100 );
function sol_scripts(){

    wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );

    wp_enqueue_script( 'slick', get_template_directory_uri() .  '/js/libs/slick.min.js', array('jquery'), null, true);
    wp_enqueue_script( 'stickybits', get_template_directory_uri() .  '/js/libs/jquery.stickybits.min.js', array('jquery'), null, true);
    wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js', array('jquery'), null, true);
    wp_enqueue_script( 'main', get_template_directory_uri() .  '/js/scripts.min.js', array('jquery'), null, true);
    wp_localize_script('main', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );

}
