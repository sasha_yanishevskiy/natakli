<?php get_header() ?>
<div class="page-content page-404 d-flex align-items-center justify-content-center flex-column">
    <h1>404</h1>
    <p>Страница удалена, либо переехала на другой адресс</p>
    <a href="<?php echo site_url() ?>">К покупкам</a>
</div>
<?php get_footer() ?>
