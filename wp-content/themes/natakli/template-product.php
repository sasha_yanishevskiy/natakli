<?php // Template name: Product  ?>
<?php get_header() ?>
<div class="page-content product">
  <div class="container max-container fluid-mobile mb-4 d-none d-lg-block">
    <div class="row">
      <div class="col-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dist">Дом бренда</a></li>
            <li class="breadcrumb-item"><a href="/dist">Каталог</a></li>
            <li class="breadcrumb-item"><a href="/dist">Категория</a></li>
            <li class="breadcrumb-item active" aria-current="page">Платья из микрофлая</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="container-fluid fluid-mobile product-wrapper">
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="product-photos">
          <div class="previews-inner">
            <ul class="previews-list d-none d-lg-block">
              <li class="anchor">
                <a href="#photo-1"> <img src="<?php echo get_template_directory_uri() ?>/img/product/product-2.jpg" alt=""> </a>
              </li>

              <li class="anchor">
                <a href="#photo-2"> <img src="<?php echo get_template_directory_uri() ?>/img/product/product-2.jpg" alt=""> </a>
              </li>

              <li class="anchor">
                <a href="#photo-3"> <img src="<?php echo get_template_directory_uri() ?>/img/product/product-2.jpg" alt=""> </a>
              </li>

              <li class="anchor">
                <a href="#photo-4"> <img src="<?php echo get_template_directory_uri() ?>/img/product/product-2.jpg" alt=""> </a>
              </li>
            </ul>
          </div>
          <ul class="product-photo-list">
            <li id="photo-1">
              <div class="pic-wrapper">
                <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
              </div>
            </li>

            <li id="photo-2">
              <div class="pic-wrapper">
                <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
              </div>
            </li>

            <li id="photo-3">
              <div class="pic-wrapper">
                <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
              </div>
            </li>

            <li id="photo-4">
              <div class="pic-wrapper">
                <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-12 col-lg-6 d-flex flex-wrap">
        <div class="product-sticky-wrapper">
          <div class="product-content">
            <form action="#" class="product-form">
              <div class="product-info">
                <div class="product-info--top">
                  <a href="#" class="collection">Дуальность</a>
                  <h1>
                    Платье из микрофая
                    <span>TB3VAQ56587AM0</span>
                  </h1>

                  <div class="order-wrapper d-md-flex align-items-end justify-content-between flex-wrap">
                    <div class="price-wrapper">
                      <p class="price--old price">€ 2 080,00</p>
                      <p class="price">€ 1 456,00</p>
                    </div>
                    <button type="submit" class="button button--bg--theme button--size--lg">
                      Добавить в корзину
                    </button>
                  </div>
                </div>

                <div class="product-info--bottom">
                  <div class="product-description" id="product-description">
                    <h2>Описание:</h2>
                    <div class="wrapper wrapper--hidden">
                      <p>Платье из микрофая с принтом Mix Flowers.</p>
                      <ul>
                        <li>
                          - Юбка с плиссировкой.
                        </li>

                        <li>
                          - Пояс с бантом.
                        </li>

                        <li>
                          - Юбка с плиссировкой.
                        </li>

                        <li>
                          - Пояс с бантом.
                        </li>
                      </ul>
                    </div>

                    <button type="button"
                            class="button button--bg--none more-button"
                            data-open-text="Скрыть" id="hidden-list-switcher">
                      Подробнее
                    </button>
                  </div>

                  <ul class="product-settings">
                    <li class="product-settings--item product-settings--item--size">
                      <h4>Размер:</h4>
                      <ul class="size-list">
                        <li>
                          <div class="radio">
                            <input type="radio" name="size" id="size_1">
                            <label for="size_1"></label>
                            <label class="theme-label" for="size_1">XS</label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input type="radio" name="size" id="size_2">
                            <label for="size_2"></label>
                            <label class="theme-label" for="size_2">S</label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input type="radio" name="size" id="size_3">
                            <label for="size_3"></label>
                            <label class="theme-label" for="size_3">M</label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input type="radio" name="size" id="size_4">
                            <label for="size_4"></label>
                            <label class="theme-label" for="size_4">L</label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input type="radio" name="size" id="size_5">
                            <label for="size_5"></label>
                            <label class="theme-label" for="size_5">XL</label>
                          </div>
                        </li>
                      </ul>
                    </li>

                    <li class="product-settings--item product-settings--item--color">
                      <h4>Цвет:</h4>
                      <ul class="colors-list">
                        <li>
                          <div class="radio">
                            <input id="color_1" type="radio" name="color" value="none">
                            <label for="color_1"></label>
                            <label class="theme-label" for="color_1"
                                   style="border: 2px solid rgb(0, 0, 0);"></label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input id="color_2" type="radio" name="color"
                                   value="#FF0000">
                            <label for="color_2"></label>
                            <label class="theme-label" for="color_2"
                                   style="background-color: rgb(255, 0, 0);"></label>
                          </div>
                        </li>

                        <li>
                          <div class="radio">
                            <input id="color_3" type="radio" name="color"
                                   value="#0085FF">
                            <label for="color_3"></label>
                            <label class="theme-label" for="color_3"
                                   style="background-color: rgb(0, 133, 255);"></label>
                          </div>
                        </li>
                        <li>
                          <div class="radio">
                            <input id="color_4" type="radio" name="color"
                                   value="#000000">
                            <label for="color_4"></label>
                            <label class="theme-label" for="color_4"
                                   style="background-color: rgb(0, 0, 0);"></label>
                          </div>
                        </li>
                      </ul>
                    </li>

                    <li class="product-settings--item">
                      <h4>
                        Количество:
                      </h4>

                      <div class="counter d-flex align-items-center" data-max-count="10" id="product-counter">
                        <button type="button" id="minus">-</button>
                        <span>
                          <input max="20" min="1" type="tel" value="1">
                        </span>

                        <button type="button" id="plus">+</button>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
            <form action="#" class="quick-order w-100">
              <h2>
                Быстрый заказ
              </h2>
              <p>Заполните простую форму, чтобы оформить быстрый заказ</p>

              <div class="row pt-3 mt-2">
                <div class="col-12 col-sm-6 col-md-7">
                  <input class="w-100" type="tel" placeholder="Номер телефона">
                </div>
                <div class="col-12 col-sm-6 col-md-5">
                  <button type="submit" class="button button--bg--theme w-100">Оформить заказ
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-12">
        <div class="already-show">
          <div class="wrap">
            <h2>Ранее вы просматривали</h2>
          </div>
          <ul class="products-list row">
            <li class="col-6 col-md-3 mb-4">
              <a href="#" class="product-card">
                <div class="product-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                </div>

                <div class="product-card--footer">
                  <span>Дуальность</span>
                  <h4>Платье из микрофлая</h4>
                  <div class="price-wrapper">
                    <div class="price price--old">€ 2 080,00</div>
                    <div class="price">€ 2 080,00</div>
                  </div>

                  <button type="button" class="button button--bg--theme">Подробнее</button>
                </div>
              </a>
            </li>

            <li class="col-6 col-md-3 mb-4">
              <a href="#" class="product-card">
                <div class="product-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                </div>

                <div class="product-card--footer">
                  <span>Дуальность</span>
                  <h4>Платье из микрофлая</h4>
                  <div class="price-wrapper">
                    <div class="price price--old">€ 2 080,00</div>
                    <div class="price">€ 2 080,00</div>
                  </div>

                  <button type="button" class="button button--bg--theme">Подробнее</button>
                </div>
              </a>
            </li>

            <li class="col-6 col-md-3 mb-4">
              <a href="#" class="product-card">
                <div class="product-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                </div>

                <div class="product-card--footer">
                  <span>Дуальность</span>
                  <h4>Платье из микрофлая</h4>
                  <div class="price-wrapper">
                    <div class="price price--old">€ 2 080,00</div>
                    <div class="price">€ 2 080,00</div>
                  </div>

                  <button type="button" class="button button--bg--theme">Подробнее</button>
                </div>
              </a>
            </li>

            <li class="col-6 col-md-3 mb-4">
              <a href="#" class="product-card">
                <div class="product-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products-list/prod-1.jpg" alt="">
                </div>

                <div class="product-card--footer">
                  <span>Дуальность</span>
                  <h4>Платье из микрофлая</h4>
                  <div class="price-wrapper">
                    <div class="price price--old">€ 2 080,00</div>
                    <div class="price">€ 2 080,00</div>
                  </div>

                  <button type="button" class="button button--bg--theme">Подробнее</button>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer() ?>

