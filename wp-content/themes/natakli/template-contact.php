<?php // Template name: Contact  ?>
<?php get_header() ?>
<div class="page-content contact">
  <div class="container max-container">
    <div class="row">
      <div class="col-12 d-none d-md-block">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dist">Дом бренда</a></li>
            <li class="breadcrumb-item active" aria-current="page">Связаться с нами</li>
          </ol>
        </nav>
      </div>

      <div class="col-12 pb-1">
        <h1>Контактные данные</h1>
      </div>
    </div>
    <div class="row mt-4 mt-lg-5">
      <div class="col-12  col-lg-6">
        <div class="contact-info">
          <div class="row">
            <div class="col-12 col-md-6">
              <div class="contact-data">
                <ul class="contact-data-list">
                  <li>
                    <span>Поддержка клиентов: </span> <a href="tel:0800888888">0 800 888 888</a>
                  </li>

                  <li>
                    <span>Заказать товар: </span> <a href="tel:0800888888">0 800 888 888</a>
                  </li>

                  <li>
                    <span>Email: </span> <a href="mailto:natakli.brand@gmail.com">natakli.brand@gmail.com</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="location-data">
                <ul class="location-data-list">
                  <li>
                    <span>Шоурум:</span>г. Харьков, ул. Сумская 99
                  </li>

                  <li>
                    <span>Время работы: </span>пн-пт с 9:00 до 19:00:
                  </li>
                </ul>
              </div>
            </div>

            <div class="col-12">
              <div class="social-wrap">
                <h3>Мы в социальных сетях</h3>
                <ul class="social">
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/instagram.svg" alt="Instagram"></a></li>
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/fb.svg" alt="Facebook"></a></li>
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/tg.svg" alt="Telegram"></a></li>
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/viber.svg" alt="Viber"></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12  col-lg-6 pl-lg-5">

        <div class="row  ">

          <div class="col-12 col-lg-11 pl-lg-5 ml-lg-auto">
            <form action="#" novalidate>
              <div class="row">
                <div class="col-12 col-md-6 col-lg-12 col-xl-6  mb-4">
                  <div class="field-wrapper">
                    <input type="text" placeholder="Имя" name="name"
                           data-empty-text="Поле не может быть пустым"
                           data-invalid-text="Имя введено некорректно"> <span
                      class="error"></span>
                  </div>
                </div>
                <div class="col-12 col-md-6 col-lg-12 col-xl-6  mb-4">
                  <div class="field-wrapper">
                    <input type="text" placeholder="Фамилия" name="surname"
                           data-empty-text="Поле не может быть пустым"
                           data-invalid-text="Фамилия введена некорректно"> <span
                      class="error"></span>
                  </div>
                </div>
                <div class="col-12 col-md-6 col-lg-12 col-xl-6  mb-4">

                  <div class="field-wrapper">
                    <input type="tel" placeholder="Номер телефона" name="phone"
                           data-empty-text="Поле не может быть пустым"
                           data-invalid-text="Телефон введен некорректно"> <span
                      class="error"></span>
                  </div>

                </div>
                <div class="col-12 col-md-6 col-lg-12 col-xl-6  mb-4">
                  <div class="field-wrapper">
                    <input type="email" placeholder="Email" name="email"
                           data-empty-text="Поле не может быть пустым"
                           data-invalid-text="Email введен некорректно"> <span
                      class="error"></span>
                  </div>
                </div>
                <div class="col-12 mb-3 mb-md-5">
                  <textarea name="message" placeholder="Ваше сообщение..."></textarea>
                </div>

                <div class="col-12 d-flex justify-content-center">
                  <button type="submit"
                          class="button button--size--lg  button--bg--theme button--bg--them">
                    Отправить запрос
                  </button>
                </div>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid px-0 mt-5 pt-3">

    <div class="row">
      <div class="col-12 col-lg-11 mx-auto">
        <div class="map-wrapper">
          <iframe width="100%"
                  src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed"
                  frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>

      </div>
    </div>
  </div>
</div>=
<?php get_footer() ?>

