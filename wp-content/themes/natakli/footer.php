</main>
<footer>

  <div class="border-inner">
    <div class="container">
      <div class="footer-wrapper">
        <div class="footer-inner clearfix">
          <div class="footer-col">
            <h4>Магазин</h4>
            <ul class="sitemap">
              <li><a href="#">Платья</a></li>
              <li><a href="#"> Жакеты</a></li>
              <li><a href="#">Брюки</a></li>
              <li><a href="#">Юбки</a></li>
              <li><a href="#">Блузы</a></li>
              <li><a href="#">Платья</a></li>
              <li><a href="#">Жилеты</a></li>
              <li><a href="#">Костюмы</a></li>
              <li><a href="#">Корсеты</a></li>
              <li><a href="#">Образы</a></li>
            </ul>
          </div>

          <div class="footer-col">
            <h4>NataKli</h4>
            <ul>
              <li><a href="#">Дом бренда</a></li>
              <li><a href="#">О бренде</a></li>
            </ul>
          </div>

          <div class="footer-col">
            <h4>Помощь </h4>
            <ul>
              <li><a href="#">Доставка и возврат</a></li>
              <li><a href="#"></a>Связаться с нами</li>
              <li><a href="#">Политика конфиденциальности</a></li>
            </ul>

            <div class="payment d-none d-lg-block mt-lg-5 pt-lg-2">
              <h5>Безопасная оплата:</h5>
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/pay/pay.svg" alt="">
            </div>
          </div>

          <div class="footer-col">
            <h4>Контакты </h4>

            <div class="row">
              <div class="col-12">
                <a href="mailto:nataklibrand@gmail.com">nataklibrand@gmail.com</a>
              </div>
              <div class="col-12">
                  <span>
                    телефон отдела продаж:
                  </span>
              </div>

              <div class="col-12">
                <a href="tel:+380962844224"> +380 (96) 28 44 224</a>
              </div>

              <div class="col-12 my-4 mb-lg-0 mt-lg-5">
                <h5>Мы в социальных сетях</h5>
                <ul class="social mt-3">
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/instagram.svg" alt="Instagram"></a></li>
                  <li><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icons/fb.svg" alt="facebook"></a></li>
                </ul>
              </div>

              <div class="col-12 pb-2 mt-2 d-lg-none">
                <div class="payment">
                  <h5>Безопасная оплата:</h5>
                  <img src="<?php echo get_template_directory_uri() ?>/img/icons/pay/pay.svg" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="copyright-wrapper">
      <div class="row">
        <div class="col-12 col-sm-6">
          <span>© NataKli 2020</span>
        </div>
        <div class="col-12 col-sm-6">
           <span>
            Сайт разработан студией <a href="#"> Muraha</a>
           </span>
        </div>
      </div>
    </div>
  </div>
</footer>


<div class="modal-wrapper">
  <aside id="cart" class="modal">
    <div class="modal-header">
      <button type="button" class="close close-modal">
        <img src="<?php echo get_template_directory_uri() ?>/img/icons/close.svg" alt="">
      </button>
    </div>

    <div class="modal-body cart">
      <div class="title">
        <h2>Корзнина</h2>
        <p>Здесь хранятся вещи, которые вы хотели купить</p>
      </div>
      <ul class="products-list">
        <li>
          <button class="close" type="button">
            <img src="<?php echo get_template_directory_uri() ?>/img/icons/close.svg" alt="">
          </button>
          <div class="sale">-30%</div>

          <!--добаить элемент с классом  "price--old" сразу перед основным блоком цены  что бы закрасить новую цену красным.-->

          <div class="cart-product">
            <div class="img-wrapper">
              <div class="cart-product--image">
                <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
              </div>
            </div>
            <div class="cart-product--info">
              <div>
                <h4>
                  Комфортный свифт-свитер </h4>

                <p class="price price--old">
                  € 2 080,00 </p>

                <p class="price">
                  € 1 456,00 </p>
              </div>

              <div class="row">
                <div class="col-12 col-md-6">
                  <span>Размер:</span> <span>XL</span>
                </div>
                <div class="col-12 col-md-6">
                  <div class="d-flex align-items-end">
                    <span>Цвет:</span> <span data-color="#000" class="color"></span>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </li>
        <li>
          <button class="close" type="button">
            <img src="<?php echo get_template_directory_uri() ?>/img/icons/close.svg" alt="">
          </button>

          <!--добаить элемент с классом  "price--old" сразу перед основным блоком цены что бы закрасить новую цену красным.-->

          <div class="cart-product">
            <div class="img-wrapper">
              <div class="cart-product--image">
                <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
              </div>
            </div>
            <div class="cart-product--info">
              <div>
                <h4>
                  Комфортный свифт-свитер </h4>

                <p class="price">
                  € 1 456,00 </p>
              </div>

              <div class="row">
                <div class="col-12 col-md-6">
                  <span>Размер:</span> <span>XL</span>
                </div>
                <div class="col-12 col-md-6 mt-1 mt-md-0">
                  <div class="d-flex align-items-end">
                    <span>Цвет:</span> <span data-color="#000" class="color"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>

      <div class="modal-footer">
        <img src="<?php echo get_template_directory_uri() ?>/img/icons/to-bottom.svg" alt="v">

        <div class="total-price row no-gutters  justify-content-between">

          <div class="col-6">
            <span>Общая стоимость</span>
          </div>
          <div class="col-6">
            <span>€ 4 992,00</span>
          </div>
          <div class="col-6">
            Стомость доставки
          </div>
          <div class="col-6">
            € 12,00
          </div>

          <div class="col-12">
            <div class="row justify-content-between">
              <div class="col-6">
                <span>К оплате</span>
              </div>
              <div class="col-6">€ 5 004,00</div>
            </div>
          </div>

        </div>  <!--  total price-->

        <button type="button" class="button show-modal button--bg--theme" data-target="#order">
          К оформлению заказа
        </button>

      </div> <!--  footer-->

    </div>

  </aside>
  <aside id="order" class="modal modal-order">
    <div class="modal-header">
      <div class="d-flex align-items-center modal-title-inner flex-wrap">
        <button type="button" class="close close-modal">
          <img src="<?php echo get_template_directory_uri() ?>/img/icons/close.svg" alt="">
        </button>

        <button class="show-modal pl-3 back-btn" data-target="#cart">
          <img src="<?php echo get_template_directory_uri() ?>/img/icons/back-black.svg" alt="">
        </button>

        <div class="title">
          <h2>Оформление заказа</h2>
          <p class="d-none d-lg-block">
            Заполните поля информацией, необходимой для отправки заказа
          </p>
        </div>
      </div>
    </div>

    <div class="order-form-wrapper">
      <div class="order-details">
        <div class="total-price row no-gutters  justify-content-between">

          <div class="col-6">
            <span>Общая стоимость</span>
          </div>
          <div class="col-6">
            <span>€ 4 992,00</span>
          </div>
          <div class="col-6">
            Стомость доставки
          </div>
          <div class="col-6">
            € 12,00
          </div>

          <div class="col-12">
            <div class="row justify-content-between">
              <div class="col-6">
                <span>К оплате</span>
              </div>
              <div class="col-6">€ 5 004,00</div>
            </div>
          </div>

        </div>
      </div>



      <form action="#" class="order-form">
        <div class="row">
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name"></div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <select class="custom-select" name="delivery" id="delivery">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <select class="custom-select" name="payment-ways" id="payment-ways">
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>
          <div class="col col-12 col-md-6 col-lg-12 col-xl-6 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>

          <div class="col-12 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>

          <div class="col-12 mb-4">
            <div class="field-wrapper">
              <input type="text" placeholder="Имя" name="name">
            </div>
          </div>

          <div class="col-12 mb-4">
            <div class="checkbox">
              <input type="checkbox" name="personal-data" id="personal-data">
              <label for="personal-data"></label>
              <label class="theme-label" for="personal-data">Я согласен на обработку моих персональных данных</label>
            </div>
          </div>

          <div class="col-12 mt-4">
            <div class="payment">
              <img src="<?php echo get_template_directory_uri() ?>/img/icons/secure-payment.png" alt="">
            </div>
          </div>

          <div class="col-12">
            <button type="submit" class="button button--bg--theme mx-auto">Оформить заказ</button>
          </div>
        </div>
      </form>
    </div>


  </aside>
</div>
<?php wp_footer() ?>
</body>
</html>
