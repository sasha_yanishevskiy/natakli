<?php // Template name: Fashion  ?>
<?php get_header() ?>
<div class="page-content fashion-page">
  <div class="container max-container fluid-mobile">
    <div class="fashion-wrapper row no-gutters">
      <div class="col-12 col-lg-6">
        <div class="fashion-item fashion-item--slider">
          <nav aria-label="breadcrumb" class="d-none d-xl-block">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/dist">Дом бренда</a></li>
              <li class="breadcrumb-item"><a href="/dist">Каталог</a></li>
              <li class="breadcrumb-item"><a href="/dist">Образ</a></li>
              <li class="breadcrumb-item active" aria-current="page">Лаконичный весенний образ
              </li>
            </ol>
          </nav>

          <ul id="fashion-page-slider" class="fashion-page-slider mt-lg-4 pt-lg-1">
            <li>
              <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo get_template_directory_uri() ?>/img/fashion-product/fashion-prod-photo-1.jpg" alt="">
            </li>
          </ul>

          <div class="d-none d-lg-block fashion-item mt-lg-5 pt-2">
            <div class="order-block">
              <form action="#">
                <div class="row">
                  <div class="col-12 col-md-6 order-2 order-md-1 mb-3 pb-2 pb-md-none mb-md-none">
                    <button class="button button--bg--theme button--order">Заказать образ</button>
                  </div>
                  <div class="col-12 col-md-6 order-1 order-md-2 mb-3 mb-md-0">
                    <div class="price-block d-flex justify-content-between align-items-center">
                      <span>Сумма</span>
                      <p class="price ml-md-auto">€ 7 072,00</p>
                    </div>
                  </div>
                </div>
              </form>

              <div class="row mt-lg-2 pt-lg-1">
                <div class="col-12 col-sm-6 mb-3 mb-sm-0">
                  Блок поделиться
                </div>
                <div class="col-12 col-sm-6 d-flex justify-content-sm-end ">
                  <a href="#" class="button button--bg--none">Как подобрать размер?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-lg-6 pr-lg-0">
        <div class="fashion-item fashion-item--fashion">
          <h1>Лаконичный весенний образ</h1>
          <ul class="fashions-list row mt-lg-3 pt-lg-1">
            <li class="col-6">
              <a href="#" class="fashions-card">
                <div class="fashions-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
                </div>
                <div class="fashions-card--content">
                  <h3>
                    Платье из микрофлая
                  </h3>

                  <div class="price-block">
                    <p class="price price--old">€ 2 080,00</p>
                    <p class="price">€ 1 456,00</p>
                  </div>
                </div>
              </a>
            </li>

            <li class="col-6 ">
              <a href="#" class="fashions-card">
                <div class="fashions-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
                </div>
                <div class="fashions-card--content">
                  <h3>
                    Платье из микрофлая
                  </h3>

                  <div class="price-block">
                    <p class="price">€ 1 456,00</p>
                  </div>
                </div>
              </a>
            </li>

            <li class="col-6">
              <a href="#" class="fashions-card">
                <div class="fashions-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
                </div>
                <div class="fashions-card--content">
                  <h3>
                    Платье из микрофлая
                  </h3>

                  <div class="price-block">
                    <p class="price price--old">€ 2 080,00</p>
                    <p class="price">€ 1 456,00</p>
                  </div>
                </div>
              </a>
            </li>

            <li class="col-6">
              <a href="#" class="fashions-card">
                <div class="sale">
                  -30%
                </div>
                <div class="fashions-card--cover">
                  <img src="<?php echo get_template_directory_uri() ?>/img/products/svitshot.png" alt="">
                </div>
                <div class="fashions-card--content">
                  <h3>
                    Платье из микрофлая
                  </h3>

                  <div class="price-block">
                    <p class="price">€ 1 456,00</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="col-12 col-lg-6 d-lg-none">
        <div class="fashion-item mt-lg-5 pt-2">
          <div class="order-block">
            <form action="#">
              <div class="row">
                <div class="col-12 col-md-6 order-2 order-md-1 mb-3 pb-2 pb-md-none mb-md-none">
                  <button class="button button--bg--theme button--order">Заказать образ</button>
                </div>
                <div class="col-12 col-md-6 order-1 order-md-2 mb-3 mb-md-0">
                  <div class="price-block d-flex justify-content-between align-items-center">
                    <span>Сумма</span>
                    <p class="price ml-md-auto">€ 7 072,00</p>
                  </div>
                </div>
              </div>
            </form>

            <div class="row mt-lg-4 pt-lg-1">
              <div class="col-12 col-sm-6 mb-3 mb-sm-0">
                Блок поделиться
              </div>
              <div class="col-12 col-sm-6 d-flex justify-content-sm-end ">
                <a href="#" class="button button--bg--none">Как подобрать размер?</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer() ?>

