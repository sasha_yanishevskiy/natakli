<?php get_header() ?>

<section class="section section--hero" id="hero">
  <ul class="slides" id="hero-slider">
    <li>
      <div class="pic-wrapper">
        <picture>
          <source srcset="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1-m.jpg" media="(max-width: 767px)">
          <img src="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1.jpg" alt="">
        </picture>
      </div>
    </li>

    <li>
      <div class="pic-wrapper">
        <picture>
          <source srcset="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1-m.jpg" media="(max-width: 767px)">
          <img src="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1.jpg" alt="">
        </picture>
      </div>
    </li>

    <li>
      <div class="pic-wrapper">
        <picture>
          <source srcset="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1-m.jpg" media="(max-width: 767px)">
          <img src="<?php echo get_template_directory_uri() ?>/img/hero-slider/slide-1.jpg" alt="">
        </picture>
      </div>
    </li>
  </ul>

  <div class="offer">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1>
            <span>NataKli <br></span> Бренд детской спортивной одежды
          </h1>
          <p>Комфорт для любой кожи </p>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="index-page">
  <section class="section section-categories">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>
            То, что создает стиль </h2>
        </div>
      </div>

      <ul class="row categories-list">
        <li class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <a href="#" class="category">
                            <span class="category-photo">
                               <img src="<?php echo get_template_directory_uri() ?>/img/categories/category-2.jpg" alt="">
                            </span>
            <div class="category-footer">
              <h3>Новинки </h3>
            </div>
          </a>
        </li>

        <li class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <a href="#" class="category">
                            <span class="category-photo">
                               <img src="<?php echo get_template_directory_uri() ?>/img/categories/category-3.jpg" alt="">
                            </span>
            <div class="category-footer">
              <h3>Платья </h3>
            </div>
          </a>
        </li>

        <li class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <a href="#" class="category">
                            <span class="category-photo">
                               <img src="<?php echo get_template_directory_uri() ?>/img/categories/category-4.jpg" alt="">
                            </span>
            <div class="category-footer">
              <h3>Свитеры и кардиганы </h3>
            </div>
          </a>
        </li>

        <li class="col-12 col-sm-6 col-lg-3">
          <a href="#" class="category">
                            <span class="category-photo">
                               <img src="<?php echo get_template_directory_uri() ?>/img/categories/category-1.jpg" alt="">
                            </span>
            <div class="category-footer">
              <h3>Пиджаки и куртки </h3>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </section>
  <section class="duality">
    <div class="container mobile-100">
      <div class="collection">
        <div class="row">
          <div class="col-12">
            <h2> Коллекция “Дуальность”</h2>
          </div>
          <div class="col-12 no-padding-mobile">
            <div class="collection-item collection-item--wide">
              <div class="collection-item--text">
                <div class="bg-text bg-text--bg--theme">
                                        <span>
                                            Не просто одежда,
                                        </span> <br> <span>
                                             а элементы личности
                                         </span>
                </div>
              </div>

              <div class="collection-content">
                <div class="collection-picture">
                  <img src="<?php echo get_template_directory_uri() ?>/img/collection/col-1.jpg" alt="">
                </div>
                <a href="#" class="button button--bg--theme">В коллекцию</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 collection-wrapper">
          <div class="collection-item collection-item--large">
            <div class="collection-content">
              <figure class="collection-picture">
                <div class="pic-wrapper">
                  <img src="<?php echo get_template_directory_uri() ?>/img/collection/col-2.jpg" alt="">
                </div>
                <figcaption>
                  <span>Твой образ дуальности </span>
                </figcaption>
                <a href="#" class="button button--bg--theme">В коллекцию</a>
              </figure>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6 collection-wrapper">
          <div class="collection-item collection-item--large">
            <div class="collection-content">
              <figure class="collection-picture">
                <div class="pic-wrapper">
                  <img src="<?php echo get_template_directory_uri() ?>/img/collection/col-3.jpg" alt="">
                </div>
                <figcaption>
                  <span>Твой образ дуальности </span>
                </figcaption>
                <a href="#" class="button button--bg--theme">В коллекцию</a>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="fashions-of-collection">
    <div class="container-fluid p-0">
      <div class="wrapper">
        <h2>
          <div class="bg-text">
                             <span>
                                Образы
                             </span> <span>
                               коллекции
                            </span>
          </div>
        </h2>
        <div class="overlay-wrapper d-none d-md-block">
          <img src="<?php echo get_template_directory_uri() ?>/img/fashion/overlay.jpg" alt="">
        </div>

        <div class="row no-gutters">

          <div class="col-12 no-gutters">
            <ul class="fashion-slider" id="fashion-slider">
              <li class="fashion-slider--item">
                <div>
                  <img src="<?php echo get_template_directory_uri() ?>/img/fashion/slide-1.jpg" alt="">
                </div>
              </li>
              <li class="fashion-slider--item">
                <div>
                  <img src="<?php echo get_template_directory_uri() ?>/img/fashion/slide-2.jpg" alt="">
                </div>
              </li>
              <li class="fashion-slider--item">
                <div>
                  <img src="<?php echo get_template_directory_uri() ?>/img/fashion/slide-3.jpg" alt="">
                </div>
              </li>
              <li class="fashion-slider--item">
                <div>
                  <img src="<?php echo get_template_directory_uri() ?>/img/fashion/slide-4.jpg" alt="">
                </div>
              </li>
              <li class="fashion-slider--item">
                <div>
                  <img src="<?php echo get_template_directory_uri() ?>/img/fashion/slide-5.jpg" alt="">
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="skew-block">
          <div class="slides-counter">
            <span id="current">1  </span>/<span id="total">10</span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-creating-style">
    <div class="container">
      <div class="row  creating-style--wrapper">
        <div class="col-12 col-sm-6">
          <h2>Создаем стиль вместе</h2>
        </div>
        <div class="col-12 col-sm-6 creating-style-block--wrapper">
          <div class="creating-style-block">
            <div class="photo">
              <img src="<?php echo get_template_directory_uri() ?>/img/create-style/create-style-1.jpg" alt="">
            </div>

            <p>
              10 луков бизнес леди </p>

            <a href="#" class="button button--bg--theme">Посмотреть</a>

          </div>
        </div>
        <div class="col-12 col-sm-6 creating-style-block--wrapper">
          <div class="creating-style-block">
            <div class="photo">
              <img src="<?php echo get_template_directory_uri() ?>/img/create-style/create-style-2.jpg" alt="">
            </div>
            <p>
              10 луков бизнес леди </p>

            <a href="#" class="button button--bg--theme">Посмотреть</a>
          </div>
        </div>

        <div class="col-12 col-sm-6 creating-style-block--wrapper">
          <div class="creating-style-block">
            <div class="photo">
              <img src="<?php echo get_template_directory_uri() ?>/img/create-style/create-style-3.jpg" alt="">
            </div>
            <p>
              10 луков бизнес леди </p>

            <a href="#" class="button button--bg--theme">Посмотреть</a>

          </div>
        </div>

        <div class="col-12 col-sm-6 creating-style-block--wrapper">
          <div class="creating-style-block">
            <div class="photo">
              <img src="<?php echo get_template_directory_uri() ?>/img/create-style/create-style-4.jpg" alt="">
            </div>
            <p>
              10 луков бизнес леди </p>

            <a href="#" class="button button--bg--theme">Посмотреть</a>

          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php get_footer() ?>
